import java.util.*;

class Cell{
  private int x;
  private int y;
  private boolean occupied;
  private Piece piece;

  //constructor
  public Cell(int x, int y){
    this.x = x;
    this.y= y;
    occupied = false;
  }

  //gettter
  public int getX(){
    return x;
  }

  public int getY(){
    return y;
  }

  public boolean getOccupation(){
    return occupied;
  }

  public Piece getPiece(){
    return piece;
  }

  //setters
  public void changeX(int x){
    this.x = x;
  }

  public void changeY(int y){
    this.y = y;
  }

  public void changeOccupation(boolean occupied){
    this.occupied = occupied;
  }

  public void changePiece(Piece piece){
    this.piece = piece;
  }


  //prints out cell
  public void printCell(){
    System.out.printf("For this cell:\n x:%d y:%d occuped:%b", this.x, this.y, this.occupied);
  }
}

class Board{
  private Cell[][] board = new Cell[8][8];
  //constructor
  public Board(){
    super();
    for(int i=0; i<8; i++){
      for(int j=0; j<8; j++){
        //creates a new cell in every location of the chess board
        board[i][j] = new Cell(i,j);
      }
    }
  }

  public Cell getCell(int x, int y){
    return board[x][y];
  }

  //prints entire Board
  public void printBoard(){
    System.out.print("    1   2   3   4   5   6   7   8\n");
    for(int i=0; i<8; i++){
      System.out.printf("%d | ", i+1);
      for(int j=0; j<8; j++){
        if(this.board[i][j].getOccupation() == false && j == 7){
          System.out.print("  |\n");
        }else{
          System.out.print("  | ");
        }
      }
    }
  }
}


public class Interface{
  public static void main(String[] args) {
    //creates a default board
    Board board = new Board();
    board.printBoard();
  }
}
