public class King extends Piece{

  public King(String team, int x, int y){
    //passes to Piece constructor
    super(team,x,y);
  }

  @overide
  public boolean isValid(Board board, int fromX, int fromY, int toX, int toY){
    if(super.isValid(board, fromX, fromY, toX, toY) == false)
      return false;
    if(Math.sqrt(Math.pow(Math.abs((toX - fromx)), 2) +Math.pow(Math.abs((toY-fromY)),2) > Math.sqrt(2) ) )
      return false;
    return true;
  }

}
