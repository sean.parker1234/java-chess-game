public class Rook extends Piece{
  public Rook(String team, int x, int y){
    //passes to super
    super(team,x,y);
  }

  @override
  public boolean isValid(Board board, int fromX, int fromY, int toX, int toY){
    if(super.isValid(board, fromX, fromY, toX, toY))
      return false  
  }
}
