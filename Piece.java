public class Piece{
  private int x;
  private int y;
  private String team;
  private boolean alive;
  private char symbol;

  public Piece(String team, int x, int y){
    super();
    this.team = team;
    this.x = x;
    this.y = y;
    symbol = 'd';
    alive = true;
  }

  //getters
  public int getX(){
    return x;
  }

  public int getY(){
    return y;
  }

  public String getTeam(){
    return team;
  }

  public boolean getAlive(){
    return alive;
  }

  //setters
  public void changeX(int x){
    this.x = x;
  }

  public void changeY(int y){
    this.y = y;
  }
  public void setAlive(boolean alive){
    this.alive = alive;
  }

  //general rule formula
  public boolean isValid(Board board, int fromX, int fromY, int toX, int toY){
    if(fromX == toX && fromY == toY){
      return false;
    }
    if(toX < 0 || toX > 7 || toY < 0 || toY > 0){
      return false;
    }
    return true;
  }

}

//public class King extends Piece
